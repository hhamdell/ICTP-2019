# ICTP - Peru -Colombia - Venezuela - Mexico 2019 workshops in Data Analysis
## This is a serie of exercises and tests for workshops under the PWF-ICTP and CEVALE2VE programs

Repo for exercises and tests for workshops (this one!)
# https://github.com/artfisica/ICTP-2019

## See [Class material](https://github.com/artfisica/ICTP-2019/tree/master/class-material) folder for details on the classes

------------------------
Created at Trieste
Docs to the repositories and other instrucctions:
https://docs.google.com/document/d/1cXCwejoF1_9NZFU0jJ-kDO63jit-glomYgKyJzPDmJE
